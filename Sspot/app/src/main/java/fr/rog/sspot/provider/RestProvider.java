package fr.rog.sspot.provider;



import org.androidannotations.rest.spring.annotations.Accept;
import org.androidannotations.rest.spring.annotations.Get;
import org.androidannotations.rest.spring.annotations.Rest;
import org.androidannotations.rest.spring.api.MediaType;

import org.springframework.http.converter.json.GsonHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;


import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import fr.rog.sspot.data.Point;

@Rest(converters = {GsonHttpMessageConverter.class})
public interface RestProvider {

    public void setRootUrl(String rootUrl);

    @Get("")
    @Accept(MediaType.APPLICATION_JSON)
    public ArrayList<String> downloadTableFromUrl();

// TODO: Debug ERROR of MMessageConverter Does care of header 'Accept'
//    @Get("")
//    @Headers({@Header(name="Accept", value = "application/xml")})
//    public Xml downloadPointFromUrl() ;


}

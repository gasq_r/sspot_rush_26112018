package fr.rog.sspot.ui;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import fr.rog.sspot.R;
import fr.rog.sspot.data.Point;

@EViewGroup(R.layout.raw_point_view)
public class RawPointView extends LinearLayout {

    @ViewById(R.id.titlePoint)
    protected TextView titlePoint;
    @ViewById(R.id.dataPoint)
    protected TextView dataPoint;

    public RawPointView bind(Point point)
    {
        titlePoint.setText("["+point.id+"]");
        dataPoint.setText(" lat.: "+point.latitude+" \n long: " + point.longitude);
        return this;
    }

    public RawPointView(Context context) {
        super(context);
    }
}

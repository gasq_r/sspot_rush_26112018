package fr.rog.sspot.data;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

import org.androidannotations.annotations.EBean;

import java.io.Serializable;

public class Point  implements Serializable, ClusterItem {
    public String id;
    public String uri;
    public float latitude;
    public float longitude;


    @Override
    public String toString() {
        super.toString();

        return id + " " + uri + " " +latitude + " " + longitude;
    }

    @Override
    public LatLng getPosition() {
        return new LatLng(latitude, longitude);
    }

    @Override
    public String getTitle() {
        return id;
    }

    @Override
    public String getSnippet() {
        return uri;
    }
}

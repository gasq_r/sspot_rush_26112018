package fr.rog.sspot.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.OptionsItem;
import org.androidannotations.annotations.OptionsMenu;
import org.androidannotations.annotations.Receiver;
import org.androidannotations.annotations.ViewById;
import java.util.ArrayList;
import java.util.Collection;

import fr.rog.sspot.R;
import fr.rog.sspot.data.Point;
import fr.rog.sspot.service.NetDataService;
import fr.rog.sspot.service.NetDataService_;
import fr.rog.sspot.ui.RawPointListViewAdapter;

@EActivity(R.layout.activity_main)
// Disable unused settings options
//@OptionsMenu({R.menu.main})
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback
{
//
//  Injected component
//

    @ViewById(R.id.drawer_layout_main)
    protected DrawerLayout drawer;

    @ViewById(R.id.toolbar)
    protected Toolbar toolbar;

    @ViewById(R.id.nav_view)
    protected NavigationView navigationView;

    @ViewById(R.id.listViewRawData)
    protected ListView listRawPoint;

    private SupportMapFragment _mapFragment;
    private GoogleMap _mMap;
    private ClusterManager _clusterMap;

    @Bean
    protected RawPointListViewAdapter adapterlistRawPoint;

//
//  Property & Method custom
//

    private final ArrayList<String> _tablePoints = new ArrayList<String>();
    private final ArrayList<String> _tablePointsPending = new ArrayList<String>();

    protected  void downloadTable(int delay)
    {
        NetDataService_.intent(getApplicationContext())
                .actionDownloadTable(this.getResources().getString(R.string.uri_points_sspot), delay)
                .start();
    }

    protected  void downloadPoint(String url, int num)
    {
        NetDataService_.intent(getApplicationContext())
                .actionDownloadPoint(url, num)
                .start();
    }

    protected void manageDownloadActions(int idAction, String name)
    {
        switch (idAction) {
            case R.id.nav_play_dl:
                if (_tablePointsPending.size() == 0)
                    downloadTable(0);
                else if (!_tablePointsPending.isEmpty()){

                    _tablePoints.addAll(_tablePointsPending);
                    _tablePointsPending.clear();
                   downloadPoint(_tablePoints.get(0), _tablePoints.size());
                }
                Toast.makeText(this, "Execute action : "+name, Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_brk_dl:
                _tablePointsPending.addAll(_tablePoints);
                _tablePoints.clear();;
                Toast.makeText(this, "Execute action : "+name, Toast.LENGTH_SHORT).show();

                break;
            case R.id.nav_stop_dl:
                _tablePoints.clear();
                _tablePointsPending.clear();
                Toast.makeText(this, "Execute action : "+name, Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
    }

    protected void manageViewModeActions(int idAction, String name)
    {
        switch (idAction) {
            case R.id.nav_list:
                Toast.makeText(this, "Execute action : "+name, Toast.LENGTH_SHORT).show();
                _mapFragment.getView().setVisibility(View.INVISIBLE);
                listRawPoint.setVisibility(View.VISIBLE);
                break;
            case R.id.nav_graph:
                Toast.makeText(this, "Execute action : "+name, Toast.LENGTH_SHORT).show();
                listRawPoint.setVisibility(View.INVISIBLE);
                _mapFragment.getView().setVisibility(View.VISIBLE);
                _clusterMap.onCameraIdle();
                break;
            default:
                break;
        }
    }


//
//    Init - Method initialization between Create & Start


    @AfterViews
    protected void initUIComponents()
    {
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        downloadTable(0);
        listRawPoint.setAdapter(adapterlistRawPoint);
        _mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        _mapFragment.getMapAsync(this);
        _mapFragment.getView().setVisibility(View.INVISIBLE);
    }

//
// OnMethod - Override UI Action by activity callback
//


    @Override
    public void onMapReady(GoogleMap googleMap) {
        _mMap = googleMap;
        _clusterMap = new ClusterManager<>(this, _mMap);
        _mMap.setOnCameraIdleListener(_clusterMap);
        _mMap.setOnMarkerClickListener(_clusterMap);
        _mMap.setOnInfoWindowClickListener(_clusterMap);
        // Add a marker in Sydney and move the camera
    }
    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            //_tablePoints.clear();
            this.finishAndRemoveTask();
        }
    }

// Disable due to unused settings option
//    @OptionsItem({ R.id.action_settings})
//    public boolean onSettings(MenuItem item)
//    {
//        Log.println(Log.DEBUG, getResources().getString(R.string.app_name), "Click on item " + item.getTitle());
//        return true;
//    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        manageDownloadActions(item.getItemId(), item.getTitle().toString());
        manageViewModeActions(item.getItemId(), item.getTitle().toString());
        if (drawer.isDrawerOpen(GravityCompat.START))
            drawer.closeDrawer(GravityCompat.START);
        return true;
    }

//
//    Broadcast Receiver - Manage communication between service & activity
//

    @Receiver(actions = fr.rog.sspot.service.NetDataService.BROADCAST_DL_TABLE_DONE)
    void broadcastDlTableDone(Intent intent)
    {
        Log.println(Log.DEBUG, getResources().getString(R.string.app_name), "Broadcast receiver download table done");
        if (intent != null && intent.hasExtra("data"))
        {
            _tablePoints.addAll(intent.getStringArrayListExtra("data"));
            if (_tablePoints.size() > 0) {
                adapterlistRawPoint.clear();
                _clusterMap.clearItems();
                downloadPoint(_tablePoints.get(0), _tablePoints.size());
            } else {
                Log.println(Log.DEBUG, getResources().getString(R.string.app_name), "Broadcast receiver retry fetch Table");
                downloadTable(NetDataService.RETRY_DOWNLOAD);
            }
        }
    }

    @Receiver(actions = fr.rog.sspot.service.NetDataService.BROADCAST_DL_POINT_DONE)
    void broadcastDlPointDone(Intent intent)
    {
        Log.println(Log.DEBUG, getResources().getString(R.string.app_name), "Broadcast receiver download point done");
        if (intent != null && intent.hasExtra("data"))
        {
            Point point = (Point)intent.getSerializableExtra("data");
            adapterlistRawPoint.add(point);
            if (listRawPoint.getVisibility() == View.VISIBLE)
                adapterlistRawPoint.notifyDataSetChanged();
            _clusterMap.addItem(point);
            if (_mapFragment.getView().getVisibility() == View.VISIBLE)
                _clusterMap.onCameraIdle();
            if (_tablePoints.size() > 0)
                _tablePoints.remove(0);
            if (_tablePoints.size() > 0) {
                downloadPoint(_tablePoints.get(0), _tablePoints.size());
            }
        }
    }

    @Receiver(actions = NetDataService.BROADCAST_DL_ERROR)
    void broadcastDlError(Intent intent)
    {
        if (intent != null && intent.hasExtra("data"))
        {
            int error = intent.getIntExtra("data", NetDataService.ERROR_FETCH_TABLE);
            if (error == NetDataService.ERROR_FETCH_TABLE ) {
                Log.println(Log.DEBUG, getResources().getString(R.string.app_name), "Broadcast receiver retry fetch Table");
                downloadTable(NetDataService.RETRY_DOWNLOAD);
            }
            if (error == NetDataService.ERROR_FETCH_POINT && _tablePoints.size() > 0) {
                Log.println(Log.DEBUG, getResources().getString(R.string.app_name), "Broadcast receiver retry fetch Table");
                String swap = _tablePoints.get(0);
                if (_tablePoints.size() > 0)
                    _tablePoints.remove(0);
                _tablePoints.add(swap);
                downloadPoint(_tablePoints.get(0), _tablePoints.size());
            }
        }
    }
}


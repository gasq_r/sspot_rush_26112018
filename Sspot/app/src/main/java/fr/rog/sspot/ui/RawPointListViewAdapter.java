package fr.rog.sspot.ui;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import java.util.ArrayList;

import fr.rog.sspot.data.Point;

@EBean
public class RawPointListViewAdapter extends BaseAdapter {

    private final ArrayList<Point> _points = new ArrayList<Point>();

    public void add(Point point)
    {
        _points.add(point);
    }

    public void clear()
    {
        _points.clear();
        this.notifyDataSetChanged();
    }

    @RootContext
    protected Context context;

    @Override
    public int getCount() {
        return _points.size();
    }

    @Override
    public Object getItem(int i) {
        if (_points.size() == 0)
            return null;
        return _points.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        RawPointView replaceView = (RawPointView)view;

        if (view == null)
            replaceView = RawPointView_.build(context);
        replaceView.bind(_points.get(i));
        return replaceView;
    }
}

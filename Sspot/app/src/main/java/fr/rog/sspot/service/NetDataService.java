package fr.rog.sspot.service;

import android.app.IntentService;
import android.content.Intent;

import android.util.Log;


import com.google.gson.Gson;

import org.androidannotations.annotations.EIntentService;
import org.androidannotations.annotations.ServiceAction;
import org.androidannotations.rest.spring.annotations.RestService;

import org.jsoup.Jsoup;

import java.net.URI;
import java.util.ArrayList;

import fr.rog.sspot.R;
import fr.rog.sspot.provider.RestProvider;
import fr.rog.sspot.data.Point;


@EIntentService
public class NetDataService extends IntentService {
    private final String TAG = this.getClass().getName();

    public static final int RETRY_DOWNLOAD = 10;

    public final static String BROADCAST_DL_TABLE_DONE = "fr.rog.spot.service.NetDataService.DL_TABLE_DONE";
    public final static String BROADCAST_DL_POINT_DONE = "fr.rog.spot.service.NetDataService.DL_POINT_DONE";
    public final static String BROADCAST_DL_ERROR = "fr.rog.spot.service.NetDataService.DL_ERROR";
    public final static int ERROR_FETCH_TABLE = 101;
    public final static int ERROR_FETCH_POINT = 102;

    private final Gson _gson = new Gson();

    @RestService
    protected RestProvider _restProvider;

    NetDataService() {
        super("NetDataService");
        Log.println(Log.DEBUG, "Ss pot", "Data Net Service built");
    }

    @ServiceAction
    protected void actionDownloadTable(String p_url, int delay) {
        // TODO: Handle action Foo
        Log.println(Log.DEBUG, TAG, "Data Net Act DL table from uri : " + p_url);
        _restProvider.setRootUrl(p_url);
        try {
            if (delay > 0 && delay <= 10)
                Thread.sleep(delay * 1000);
            ArrayList<String> table = _restProvider.downloadTableFromUrl();
            Log.println(Log.DEBUG, TAG, "Downloaded table of uri ");
            sendBroadcast(new Intent(BROADCAST_DL_TABLE_DONE).putExtra("data", table));
        } catch (Exception error) {
            Log.println(Log.DEBUG, TAG, error.getMessage());
            sendBroadcast(new Intent(BROADCAST_DL_ERROR).putExtra("data", ERROR_FETCH_TABLE));
        }

    }

    @ServiceAction
    protected void actionDownloadPoint(String p_urls, int num) {
        // TODO: Handle action Foo
        Log.println(Log.DEBUG, TAG, "Data Net Act DL point from table ");
        if (num > 0) {
            try {
                if (num % 10 == 1)
                    Thread.sleep(1000);
            } catch (Exception error) {
                Log.println(Log.DEBUG, TAG, "Exceptionwhile waiting to the n°" + num + " uris");
                sendBroadcast(new Intent(BROADCAST_DL_ERROR).putExtra("data", ERROR_FETCH_POINT));
            }
            try {
                Point point = _gson.fromJson(Jsoup.connect(p_urls).get().body().text(), Point.class);
                point.uri = p_urls;
                String arr[] = p_urls.split("/");
                point.id = arr[arr.length-1];
                Log.println(Log.DEBUG, TAG, "Download point for uri " + p_urls+ " of num "+num+" & = "+point);
                sendBroadcast(new Intent(BROADCAST_DL_POINT_DONE).putExtra("data", point));
            } catch (Exception error) {
                Log.println(Log.DEBUG, TAG, error.getMessage());
                sendBroadcast(new Intent(BROADCAST_DL_ERROR).putExtra("data", ERROR_FETCH_POINT));

            }

        }
    }

    @Override
    protected void onHandleIntent(Intent intent) {
    }
}

